package com.example.riadibrahim.testapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText screen;
    TextView statementScreen;
    btnOnClick Btn;

    String operation = "";
    boolean clearAll = false;
    float firstNumber = 0;
    float secondNumber = 0;
    float finalResult = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        screen = (EditText)findViewById(R.id.calculatorScreen);
        screen.setEnabled(false);

        statementScreen = (TextView)findViewById(R.id.statementShow);

        final Button btnOne = (Button)findViewById(R.id.btn1);

        Btn = new btnOnClick();

        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String valueOfBtnOne = btnOne.getText().toString();
                screen.setText(valueOfBtnOne);
            }
        });

        int [] btnList = {R.id.btn0,R.id.btn00,R.id.btn1,R.id.btn2,R.id.btn3,R.id.btn4,R.id.btn5,R.id.btn6,R.id.btn7,R.id.btn8,R.id.btn9,R.id.btnDot,R.id.btnAdd,R.id.btnSub,R.id.btnMul,R.id.btnDiv,R.id.btnEql,R.id.btnAc,R.id.btnC,R.id.btnBackspace};

        for(int id : btnList){
            View view = findViewById(id);
            view.setOnClickListener(Btn);
        }

        //long press in backspace
        Button btnBackspace = (Button)findViewById(R.id.btnBackspace);
        btnBackspace.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                firstNumber = 0;
                secondNumber = 0;
                screen.setText("0");
                operation = "";
                statementScreen.setText("0");
                return false;
            }
        }) ;
    }



    //button click handler;
    public class btnOnClick implements View.OnClickListener{
        public void onClick(View view){
            switch (view.getId()){
                case R.id.btnAc:
                    firstNumber = 0;
                    secondNumber = 0;
                    screen.setText("0");
                    operation = "";
                    statementScreen.setText("0");
                    break;
                case R.id.btnC:
                    secondNumber = 0;
                    screen.setText("0");
                    break;
                case R.id.btnBackspace:
                    backspaceButton();
                    break;
                case R.id.btnAdd:
                    math("+");
                    break;
                case R.id.btnSub:
                    math("-");
                    break;
                case R.id.btnMul:
                    math("*");
                    break;
                case R.id.btnDiv:
                    math("/");
                    break;
                case R.id.btnEql:
                    clearAll = true;
                    Result();
                    break;
                default:
                    String Number = ((Button)view).getText().toString();
                    keyboard(Number);
                    break;
            }
        }
    }

    public void keyboard(String character){

        if(clearAll){
            screen.setText("0");
            clearAll = false;
        }

        String currentScreen = screen.getText().toString();

        if(currentScreen.equals("0") || currentScreen.equals("+") || currentScreen.equals("-") || currentScreen.equals("*") || currentScreen.equals("/") || currentScreen.equals("Invalid input")) {
            currentScreen = "";
        }

        if(character.equals(".")){
            //check dot is exists or not;
            String[] characters = {"."};
            boolean dotNotExists = checkDuplicateCharacter(characters);

            //if dot exists it will show current screen
            if(dotNotExists){
                currentScreen += character;
            }
        }else{
            currentScreen += character;
        }
        screen.setText(currentScreen);
    }


    public void math(String operator){
        String current = screen.getText().toString();

        if(current.equals("Invalid input") || current.equals("Undefined") || current.equals(".")){
            screen.setText(operator);
        }else{
            //Check operator sign already exists or not
            String[] characters = {"+","-", "*", "/"};
            boolean characterNotExists = checkDuplicateCharacter(characters);
            if(characterNotExists) {
                firstNumber = Float.parseFloat(screen.getText().toString());
                operation = operator;
            }
            operation = operator;
            screen.setText(operator);
        }
        statementScreen.setText(String.valueOf(firstNumber)+operation);
    }

    public void Result(){
        String currentScreen = screen.getText().toString();

        //if user click = button while operator in screen
        if(currentScreen.equals("+") || currentScreen.equals("-") || currentScreen.equals("*") || currentScreen.equals("/") || currentScreen.equals("Invalid input") || currentScreen.equals(".") || currentScreen.equals("Undefined")){
            screen.setText("Invalid input");
        }else{
            secondNumber = Float.parseFloat(currentScreen);

            if(operation.equals("+") || operation.equals("-") || operation.equals("*") || operation.equals("/")){
                if(operation.equals("+")){
                    finalResult = firstNumber + secondNumber;
                }else if(operation.equals("-")){
                    finalResult = firstNumber - secondNumber;
                }else if(operation.equals("*")){
                    finalResult = firstNumber * secondNumber;
                }else if(operation.equals("/")){
                    finalResult = firstNumber / secondNumber;
                }

                //if user 0 divide by 0 this condition show undefined;
                if(firstNumber == 0 && secondNumber == 0 && operation.equals("/")){
                    screen.setText("Undefined");
                }else{
                    screen.setText(String.valueOf(finalResult));
                }
                statementScreen.setText(String.valueOf(firstNumber)+operation+String.valueOf(secondNumber));
            }else{
                statementScreen.setText("0");
                screen.setText("0");
            }
            operation = "clear"; //this line only for change the value of operation variable;
        }
    }

    //this method will delete last digit of screen;
    public void backspaceButton(){

        String currentScreen = screen.getText().toString();

        int screenCharacterLength = currentScreen.length();

        if(screenCharacterLength > 1 && !currentScreen.equals("0") && !currentScreen.equals("Undefined") && !currentScreen.equals("Invalid input") && !currentScreen.equals("Infinity")){
            String afterCharacterDelete = currentScreen.substring(0, screenCharacterLength-1);
            screen.setText(afterCharacterDelete);
        }else {
            screen.setText("0");
        }
    }

    //this method check duplicate character;
    public boolean checkDuplicateCharacter(String[] characters){

        String currentScreen = screen.getText().toString();

        int count = 0;

        for(String character : characters){
            if(currentScreen.contains(character)){
                count++;
            }
        }

        if(count == 0){
            return true;
        }else{
            return false;
        }
    }
}
